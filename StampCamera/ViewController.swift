//
//  ViewController.swift
//  StampCamera
//
//  Created by Yukinaga Azuma on 2016/04/02.
//  Copyright © 2016年 tekuru. All rights reserved.
//

import UIKit
import Social
import GoogleMobileAds
import AVFoundation

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet var imageView:UIImageView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewWillAppear(animated: Bool) {
        let delegate =  UIApplication.sharedApplication().delegate as! AppDelegate
        if delegate.isStampAdded {
            delegate.isStampAdded = false
            
            let stamp = Stamp()
            let stampSize:CGFloat = 100
            stamp.frame = CGRectMake(0, 0, stampSize, stampSize)
            stamp.center = self.view.center
            stamp.contentMode = .ScaleAspectFit
            stamp.image = delegate.image
            stamp.userInteractionEnabled = true
            self.view.addSubview(stamp)
        }
    }
    
    @IBAction func camera(sender:AnyObject){
        let pickerController = UIImagePickerController()
        pickerController.sourceType = .PhotoLibrary
        pickerController.delegate = self
        self.presentViewController(pickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imageView.image = info["UIImagePickerControllerOriginalImage"] as? UIImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func deleteTapped(sender:AnyObject) {
        let subView = self.view.subviews.last!
        if subView.isKindOfClass(Stamp) {
            subView.removeFromSuperview()
        }
    }
    
    @IBAction func snsTapped(){
        UIGraphicsBeginImageContextWithOptions(self.view.frame.size, self.view.opaque, 0.0)
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let savedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        UIImageWriteToSavedPhotosAlbum(savedImage!, self, nil, nil)
        
        let myComposeView = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        myComposeView.setInitialText("Stamp Camera!")
        myComposeView.addImage(savedImage)
        self.presentViewController(myComposeView, animated: true, completion: nil)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Google Mobile Ads SDK version: \(GADRequest.sdkVersion())")
        bannerView.adUnitID = "ca-app-pub-4387267724448126/9684783491"
        bannerView.rootViewController = self
        //let admobRequest:GADRequest = GADRequest()
        bannerView.loadRequest(GADRequest())

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

