//  Copyright © 2016年 tekuru. All rights reserved.
//

import UIKit

class Stamp: UIImageView, UIGestureRecognizerDelegate {
    
    var currentTransform:CGAffineTransform!
    var scale: CGFloat = 1.0
    var angle: CGFloat = 0
    var isMoving: Bool = false
    
    override func didMoveToSuperview() {
        let rotationRecognizer: UIRotationGestureRecognizer = UIRotationGestureRecognizer(target:self, action:#selector(Stamp.rotationGesture(_:)))
        rotationRecognizer.delegate = self
        self.addGestureRecognizer(rotationRecognizer)
        
        let pinchRecognizer: UIPinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(Stamp.pinchGesture(_:)))
        pinchRecognizer.delegate = self
        self.addGestureRecognizer(pinchRecognizer)
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func rotationGesture(gesture: UIRotationGestureRecognizer){
        print("Rotation detected!", terminator: "")
        
        if !isMoving && gesture.state == UIGestureRecognizerState.Began {
            isMoving = true
            currentTransform = self.transform
        }else if isMoving && gesture.state == UIGestureRecognizerState.Ended {
            isMoving = false
            scale = 1.0
            angle = 0.0
        }
        
        angle = gesture.rotation
        
        let transform = CGAffineTransformConcat(CGAffineTransformConcat(currentTransform,CGAffineTransformMakeRotation(angle)), CGAffineTransformMakeScale(scale, scale))
        
        self.transform = transform
        
    }
    
    func pinchGesture(gesture: UIPinchGestureRecognizer){
        print("Pinch detected!", terminator: "")
        
        if !isMoving && gesture.state == UIGestureRecognizerState.Began {
            isMoving = true
            currentTransform = self.transform
        }else if isMoving && gesture.state == UIGestureRecognizerState.Ended {
            isMoving = false
            scale = 1.0
            angle = 0.0
        }
        
        scale = gesture.scale
        
        let transform = CGAffineTransformConcat(CGAffineTransformConcat(currentTransform, CGAffineTransformMakeRotation(angle)), CGAffineTransformMakeScale(scale, scale))
        
        self.transform = transform;
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.superview?.bringSubviewToFront(self)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first
        let location = touch?.locationInView(self.superview)
        let preLocation = touch?.previousLocationInView(self.superview)
        let dx = location!.x - preLocation!.x
        let dy = location!.y - preLocation!.y
        self.center = CGPointMake(self.center.x + dx, self.center.y + dy)
    }
    
}