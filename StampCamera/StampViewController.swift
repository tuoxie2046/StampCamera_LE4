//
//  StampViewController.swift
//  StampCamera
//
//  Created by Yukinaga Azuma on 2016/04/09.
//  Copyright © 2016年 tekuru. All rights reserved.
//

import UIKit


class StampViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let stampArray = [
        UIImage(named: "1.png"),
        UIImage(named: "2.png"),
        UIImage(named: "3.png"),
        UIImage(named: "4.png"),
        UIImage(named: "5.png")
    ]
    
    func  collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return stampArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = stampArray[indexPath.row]
        return cell;
    }
    
    func  collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let delegate =  UIApplication.sharedApplication().delegate as! AppDelegate
        delegate.image = stampArray[indexPath.row]
        delegate.isStampAdded = true
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func close(sender:AnyObject){
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
